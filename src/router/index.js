import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Node from '../views/node/Node.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/node/:node_id',
    name: 'Node',
    component: Node
    //component: () => import('../views/node/Node.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  { path: '*', redirect: '/' }
]

const router = new VueRouter({
    routes: routes,
})

export default router
