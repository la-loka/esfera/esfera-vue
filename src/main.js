/*
“Copyright 2020 L'esfera”

This file is part of L'esfera.

L'esfera is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'

export const eventBus = new Vue();

Vue.config.productionTip = false

//https://seregpie.github.io/VueWordCloud/
//https://github.com/code-farmer-i/vue-markdown-editor

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
