# Frontend installation

L'esfera has two parts.
* The server. Written with the FastAPI python framework.
* The frontend (this document). Written in Vuejs2

# Production installation

You will need a web server like nginx or apache2

```bash
apt-get install nginx
```

## Get the code

```bash
cd /var/www/
mkdir ./esfera
wget https://gitlab.com/la-loka/esfera/esfera-vue/-/archive/master/esfera-vue-master.tar.gz
tar zxvf esfera-vue-master.tar.gz --strip-components=1 -C esfera
```

## Configure an nginx host

Edit `/etc/hosts` and add an entry to point to the esfera-server
```bash
127.0.0.1	esfera-server
```

Edit a new nginx hosts file
```bash
server {
    listen         80;
    server_name    my_domain.com;
    return         301 https://$server_name$request_uri;
}
server {
    listen 443 ssl;    
    server_name my_domain.com;

    ssl_certificate           /etc/letsencrypt/live/my_domain.com/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/my_domain.com/privkey.pem;
    ssl_dhparam               /etc/letsencrypt/ssl-dhparams.pem;

    location / {
        root   /var/www/esfera/dist;
        index  index.html;
    }
    location = /favicon.ico {
        alias /var/www/esfera/dist/favicon.ico;
    }

    location /api/ {
        proxy_pass          http://esfera-server:5050/;
        proxy_set_header    Host    $host;
        proxy_set_header    X-Forwarded-For $remote_addr;
        proxy_set_header    X-Real-IP   $remote_addr;
        proxy_pass_header   server;        
    }
    access_log /var/log/nginx/esfera.access.log;
    error_log /var/log/nginx/esfera.error.log notice;
}
```

# Development installation

You will need node and npm installed

## Get the code
```bash
git clone https://gitlab.com/la-loka/esfera/esfera-vue
or
git clone git@gitlab.com/la-loka/esfera/esfera-vue
```

Create `.env.development` to define the esfera api server

```bash
VUE_APP_API_SERVER="http://localhost:5050"
```

### Compiles and hot-reloads for development
```bash
npm run serve
```

### Compiles and minifies for production
```bash
npm run build
```

### Lints and fixes files
```bash
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
